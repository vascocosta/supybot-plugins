#!/bin/bash

DATABASE='/home/gluon/var/irc/bots/Senna/data/Motorsport.db'
DATABASE_BACKUP='/home/gluon/var/irc/bots/Senna/backup/Motorsport.db.bak'
ICAL2SQLITE='python ical2sqlite.py'
LOG_FILE='/home/gluon/var/irc/bots/Senna/plugins/Motorsport/tools/update_events.log'
MAX_TRIES=5
WAIT_PERIOD=10
WORK_DIR='/home/gluon/var/irc/bots/Senna/plugins/Motorsport/tools/'

declare -A CALENDARS

#CALENDARS[All]='http://rmotorsportscalendar.com/calendar/bafd5733ab.ics'
CALENDARS[All]='https://calendar.google.com/calendar/ical/fvmqbcdb3orbf489uva4nqqof4%40group.calendar.google.com/private-4f4c563107e8ea344bc4df23e253db77/basic.ics'

cd $WORK_DIR
success=1
tries=$MAX_TRIES
while [ $tries -gt 0 ] && [ $success -ne 0 ]; do
    success=0
    for i in "${!CALENDARS[@]}"
    do
        wget --quiet --timeout=60 --tries=1 --output-document $i.ics ${CALENDARS[$i]}
        if [ $? -ne 0 ]; then
            success=1
            rm -f *.ics *.sql
            echo "[$(date)] Error: Problem fetching $i calendar. Nothing changed." >> $LOG_FILE
            break
        fi
	# Normalise DTSTART;VALUE=DATE entries to regular DTSTART times by setting T000000Z as the time
        sed -i '/DTSTART;VALUE=DATE/ s/$/T000000Z/' $i.ics
        sed -i 's/DTSTART;VALUE=DATE/DTSTART/g' $i.ics
        sed -i 's///g' $i.ics
        $ICAL2SQLITE $i.ics $i.sql
        if [ $? -ne 0 ]; then
            success=1
            rm -f *.ics *.sql
            echo "[$(date)] Error: Problem converting $i calendar. Nothing changed." >> $LOG_FILE
            break
        fi
    done
    sleep $WAIT_PERIOD
    let tries=tries-1
done
if [ $success -eq 0 ]; then
    sqlite3 $DATABASE 'DROP TABLE IF EXISTS events;'
    for i in "${!CALENDARS[@]}"
    do
        sqlite3 $DATABASE < $i.sql
        if [ $? -ne 0 ]; then
            cp $DATABASE_BACKUP $DATABASE
            rm -f *.ics *.sql
            echo "[$(date)] Error: Problem importing $i calendar. Database restored from backup." >> $LOG_FILE
            exit 1
        fi
    done
    rm -f *.ics *.sql
    echo "[$(date)] Database updated." >> $LOG_FILE
fi
