import sqlite3

current_race = '[formula 1] abu dhabi gp'
database_path = '/home/gluon/var/irc/bots/Senna/data/Motorsport.db'
ergast_url = 'http://ergast.com/api/f1/'
processed_bets_path = '/home/gluon/var/irc/bots/Senna/data/processed_bets.flat'

def bets(race='last'):
    bets = []
    try:
        conn = sqlite3.connect(database_path)
        cursor = conn.cursor()
        sql = 'SELECT id, first, second, third, user_id from bets ' \
              'WHERE race = ? COLLATE NOCASE'
        cursor.execute(sql, (race.lower(),))
        results = cursor.fetchall()
        if results:
            for i in results:
                bets.append({'id': i[0], 'first': i[1], 'second': i[2], 'third': i[3], 'bet_points': 0})
            return bets
        else:
            return None
    except sqlite3.Error:
        print("Error accessing the database.")
        return
    finally:
        conn.close() 

def update_points(bets, results):
    for i in bets:
        if i['first'].lower() in (results['1'], results['2'], results['3']):
            if i['first'].lower() == results['1']:
                i['bet_points'] += 10
            else:
                i['bet_points'] += 5
        if i['second'].lower() in (results['1'], results['2'], results['3']):
            if i['second'].lower() == results['2']:
                i['bet_points'] += 10
            else:
                i['bet_points'] += 5
        if i['third'].lower() in (results['1'], results['2'], results['3']):
            if i['third'].lower() == results['3']:
                i['bet_points'] += 15
            else:
                i['bet_points'] += 5
        try:
            conn = sqlite3.connect(database_path)
            cursor = conn.cursor()
            sql = 'UPDATE bets SET bet_points = ? ' \
                  'WHERE id = ?'
            cursor.execute(sql, (i['bet_points'], i['id']))
            conn.commit()
        except sqlite3.Error:
            print("Error accessing the database.")
            return
        finally:
            conn.close()

bets = bets(current_race)
results = {'1': 'ham', '2': 'vet', '3': 'ver'}
update_points(bets, results)

# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=100:
