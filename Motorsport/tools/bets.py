import requests
import sqlite3

current_race = '[formula 1] abu dhabi gp'
database_path = '/home/gluon/var/irc/bots/Senna/data/Motorsport.db'
ergast_url = 'http://ergast.com/api/f1/'
processed_bets_path = '/home/gluon/var/irc/bots/Senna/data/processed_bets.flat'

def bets(race='last'):
    bets = []
    try:
        conn = sqlite3.connect(database_path)
        cursor = conn.cursor()
        sql = 'SELECT nick, first, second, third, points from bets INNER JOIN users ' \
              'ON bets.user_id = users.id ' \
              'WHERE race = ? COLLATE NOCASE'
        cursor.execute(sql, (race.lower(),))
        results = cursor.fetchall()
        if results:
            for i in results:
                bets.append({'nick': i[0], 'first': i[1], 'second': i[2], 'third': i[3], 'points': i[4]})
            return bets
        else:
            return None
    except sqlite3.Error:
        print("Error accessing the database.")
        return
    finally:
        conn.close() 

def results(season='current', race='last'):
    results = {}
    r = requests.get(ergast_url + season + '/' + race + '/' + 'results' +'.json')
    try:
        r = r.json()
    except ValueError:
        print("Invalid year, round or session.")
        return
    for i in r['MRData']['RaceTable']['Races'][0]['Results']:
        results[i['position']] = i['Driver']['code'].lower()
    return results

def update_points(bets, results):
    for i in bets:
        if i['first'].lower() in (results['1'], results['2'], results['3']):
            if i['first'].lower() == results['1']:
                i['points'] += 10 
            else:
                i['points'] += 5
        if i['second'].lower() in (results['1'], results['2'], results['3']):
            if i['second'].lower() == results['2']:
                i['points'] += 10
            else:
                i['points'] += 5
        if i['third'].lower() in (results['1'], results['2'], results['3']):
            if i['third'].lower() == results['3']:
                i['points'] += 15
            else:
                i['points'] += 5
        try:
            conn = sqlite3.connect(database_path)
            cursor = conn.cursor()
            sql = 'UPDATE users SET points = ? ' \
                  'WHERE nick = ?'
            cursor.execute(sql, (i['points'], i['nick']))
            conn.commit()
        except sqlite3.Error:
            print("Error accessing the database.")
            return
        finally:
            conn.close()

with open(processed_bets_path, 'r+a') as f:
    data = f.read()
    if current_race not in data:
        bets = bets(current_race)
        #results = results()
        results = {'1': 'ham', '2': 'vet', '3': 'ver'}
        update_points(bets, results)
        f.write(current_race + '\n')
    else:
        print("Current race already processed.")

# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=100:
