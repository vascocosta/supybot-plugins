###
# Copyright (c) 2017, Vasco Costa
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import supybot.conf as conf
import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
try:
    from supybot.i18n import PluginInternationalization
    _ = PluginInternationalization('Motorsport')
except ImportError:
    # Placeholder that allows to run the plugin on a bot
    # without the i18n module
    _ = lambda x: x

import calendar
import datetime
import praw
import pytz
import random
import re
import requests
import sqlite3


class Motorsport(callbacks.Plugin):
    """This plugin provides commands related to Motorsport."""

    def __init__(self, irc):
        self.__parent = super(Motorsport, self)
        self.__parent.__init__(irc)
        self.ergast_url = 'http://ergast.com/api/f1/'
        self.answers_path = conf.supybot.directories.data.dirize('answers.flat')
        self.database_path = conf.supybot.directories.data.dirize('Motorsport.db')
        self.events_announced_path = conf.supybot.directories.data.dirize('events_announced.flat')
        self.kimi_memes_path = conf.supybot.directories.data.dirize('kimi_memes.flat')
        self.videos_announced_path = conf.supybot.directories.data.dirize('videos_announced.flat')
        self.timezone = self.registryValue('timezone')
        if not self.timezone:
            self.timezone = 'UTC'
        self.client_id = self.registryValue('clientID')
        if not self.client_id:
            irc.error("No Reddit client ID set.", Raise=True)
        self.client_secret = self.registryValue('clientSecret')
        if not self.client_secret:
            irc.error("No Reddit client secret set.", Raise=True)
        self.user_agent = self.registryValue('userAgent')
        if not self.user_agent:
            irc.error("No Reddit user agent set.", Raise=True)

    def _current_race(self):
        try:
            conn = sqlite3.connect(self.database_path)
            cursor = conn.cursor()
            sql = 'SELECT description, dtstart, summary FROM events ' \
                  'WHERE dtstart > datetime("now") AND ' \
                  'summary LIKE ? ' \
                  'ORDER BY dtstart LIMIT 1'
            cursor.execute(sql, ('%formula 1%race%',))
            result = cursor.fetchone()
        except sqlite3.Error:
            irc.error("Problem accessing the database.", Raise=True)
        finally:
            conn.close()
        if not result:
            irc.error("Could not find the current race.", Raise=True)
        current_race = result[0][6:-5]
        return current_race

    def _delta(self, date_string):
        t2 = datetime.datetime.strptime(date_string, '%Y-%m-%d %H:%M:%S UTC')
        t1 = datetime.datetime.utcnow()
        delta = t2 - t1
        result = {}
        result['days'] = delta.days
        result['hours'] = delta.seconds // 3600
        result['minutes'] = (delta.seconds // 60) % 60
        return result

    def _lookup(self, name, type):
        if type.lower() == 'category':
            category_aliases = self.registryValue('categoryAliases')
            if not category_aliases:
                irc.error("No category aliases set.", Raise=True)
            split = dict(item.split(':') for item in category_aliases.split(';'))
            if name.lower() in split.keys():
                return split[name.lower()]
            else:
                return name.lower()
        elif type.lower() == 'event':
            event_aliases = self.registryValue('eventAliases')
            if not event_aliases:
                irc.error("No event aliases set.", Raise=True)
            split = dict(item.split(':') for item in event_aliases.split(';'))
            if name.lower() in split.keys():
                return split[name.lower()]
            else:
                return name.lower()
        elif type.lower() == 'driver':
            driver_aliases = self.registryValue('driverAliases')
            if not driver_aliases:
                irc.error("No driver aliases set.", Raise=True)
            split = dict(item.split(':') for item in driver_aliases.split(';'))
            if name.lower() in split.keys():
                return split[name.lower()]
            else:
                return name.lower()

    def _valid_drivers(self, season, drivers):
        r = requests.get(self.ergast_url + str(season) + '/' + 'drivers.json')
        results = r.json()
        valid_drivers = [False] * len(drivers)
        for index, i in enumerate(drivers):
            for j in results['MRData']['DriverTable']['Drivers']:
                if i.lower() == j['code'].lower() or i.lower() == j['familyName'].lower():
                    valid_drivers[index] = True
        return valid_drivers

    def ace(self, irc, msg, args, category):
        """[category]

        Returns acestreams for motorsport events.
        Examples: !ace | !ace list | !ace formula 1 | !ace indycar | !ace wec | !ace motogp
        """
        try:
            conn = sqlite3.connect(self.database_path)
            cursor = conn.cursor()
            if category and category.lower().startswith('default'):
                split_category = category.lower().split(' ')
                if len(split_category) == 3:
                    sql = 'UPDATE acestream_links set link = ? WHERE category = ?'
                    cursor.execute(sql, (split_category[2], split_category[1]))
                    conn.commit()
                    irc.reply("Default link updated.")
                    return
                else:
                    irc.error("Invalid category or link", Raise=True)
            else:
                sql = 'SELECT category, link FROM acestream_links ' \
                      'WHERE link != "0" ' \
                      'LIMIT 5'
                cursor.execute(sql)
                results = cursor.fetchall()
        except sqlite3.Error:
            irc.error("Problem accessing the database.", Raise=True)
        finally:
            conn.close()
        reddit = praw.Reddit(client_id=self.client_id,
                             client_secret=self.client_secret,
                             user_agent=self.user_agent)
        p = re.compile('[a-z0-9]{40}', re.MULTILINE)
        listing = False
        if category and category != 'list':
            category = self._lookup(category, 'category')
        else:
            category = 'list'
            listing = True
        empty = True
        if listing:
            irc.reply(ircutils.bold("Current stream list (.ace string_on_list to get the links):"))
        else:
            irc.reply(ircutils.bold("Current streams:"))
        for submission in reddit.subreddit('motorsportsstreams').new(limit=20):
            if category in submission.title.lower() or category == 'list':
                streams = []
                m = p.findall(submission.selftext)
                if m:
                    streams.extend(m)
                for comment in submission.comments:
                    m = p.findall(comment.body)
                    if m:
                        streams.extend(m)
                if streams:
                    empty = False
                    streams = list(set(streams))
                    irc.reply(submission.title)
                    if category != 'list':
                        for stream in streams:
                            irc.reply(stream)
        if empty:
            irc.reply("No current streams found.")
        if results:
            irc.reply(ircutils.bold("Default streams:"))
            for i in results:
                irc.reply(i[0][0].upper() + i[0][1:] + " - " + i[1])

    def ask(self, irc, msg, args, question):
        """<question>

        Answers your question.
        Examples: !ask Will Ericsson win the race? | !ask Will there be drama into turn 1?
        """
        with open(self.answers_path, 'r') as f:
            answers = f.read().splitlines()
        irc.reply(answers[random.randint(0, len(answers)-1)])

    def kimi(self, irc, msg, args):
        """

        Random Kimi shit.
        Examples: !kimi
        """
        with open(self.kimi_memes_path, 'r') as f:
            kimi_memes = f.read().splitlines()
        irc.reply(kimi_memes[random.randint(0, len(kimi_memes)-1)])

    def bet(self, irc, msg, args, first=None, second=None, third=None):
        """[first] [second] [third]

        Shows your current bet, bet log or places a new bet.
        Examples: !bet | !bet log | !bet ham bot vet
        """
        if first == 'log' and second == third == None: 
            try:
                conn = sqlite3.connect(self.database_path)
                cursor = conn.cursor()
                sql = 'SELECT first, second, third, race, nick, users.id, bet_points, bets.id from bets INNER JOIN users ' \
                      'ON bets.user_id = users.id ' \
                      'WHERE nick = ? ' \
                      'COLLATE NOCASE ORDER BY bets.id DESC LIMIT 5'
                cursor.execute(sql, (msg.nick.lower(),))
                result = cursor.fetchall()
                if result:
                    for i in reversed(result):
                        irc.reply("Your bet for the " + \
                                  ircutils.bold(i[3][12].upper() + \
                                  i[3][13:len(i[3])-3] + " GP: ") + \
                                  i[0].upper() + " " + \
                                  i[1].upper() + " " + \
                                  i[2].upper() + " " + \
                                  str(i[6]) + " points.")
                else:
                    irc.reply("You have never placed a bet.")
            except sqlite3.Error:
                irc.error("Problem accessing the database.", Raise=True)
            finally:
                conn.close()
            return
        if self.registryValue('betsClosed'):
            irc.reply("Bets are currently closed.")
            return
        if (all((first, second, third)) and 
            (len((first, second, third)) != len(set((first, second, third))) or not
            all(self._valid_drivers('current', (first, second, third))))):
            irc.error("Invalid drivers.", Raise=True)
        try:
            conn = sqlite3.connect(self.database_path)
            cursor = conn.cursor()
            sql = 'SELECT dtstart, summary FROM events ' \
                  'WHERE dtstart > datetime("now") AND ' \
                  'summary LIKE ? ' \
                  'ORDER BY dtstart LIMIT 1'
            cursor.execute(sql, ('%formula 1%qualifying%',))
            result = cursor.fetchone()
        except sqlite3.Error:
            irc.error("Problem accessing the database.", Raise=True)
        finally:
            conn.close()
        if not result:
            irc.error("Could not find the current race.", Raise=True)
        race_time = datetime.datetime.strptime(result[0], '%Y-%m-%d %H:%M:%S UTC')
        race = result[1][:-11]
        try:
            conn = sqlite3.connect(self.database_path)
            cursor = conn.cursor()
            sql = 'SELECT first, second, third, race, nick, users.id from bets INNER JOIN users ' \
                  'ON bets.user_id = users.id ' \
                  'WHERE nick = ? AND ' \
                  'race = ? COLLATE NOCASE'
            cursor.execute(sql, (msg.nick.lower(), race.lower()))
            result = cursor.fetchone()
            if result:
                user_id = result[5]
                if not all((first, second, third)):
                    if first != None:
                        irc.error("Invalid drivers.", Raise=True)
                    irc.reply("Your " + ircutils.bold("current") + \
                              " bet for the " + ircutils.bold(race[12:len(race)-3] + " GP: ") + \
                              result[0].upper() + " " + \
                              result[1].upper() + " " + \
                              result[2].upper() + ".")
                    return
        except sqlite3.Error:
            irc.error("Problem accessing the database.", Raise=True)
        finally:
            conn.close()
        if not result:
            # No bet found for the current race. Insert new one.
            if not all((first, second, third)):
                if first != None:
                    irc.error("Invalid drivers.", Raise=True)
                irc.error("You haven't placed a bet for the " + \
                          ircutils.bold(race[12:len(race)-3] + " GP ") + "yet.", Raise=True)
            try:
                conn = sqlite3.connect(self.database_path)
                cursor = conn.cursor()
                sql = 'SELECT id from users WHERE nick = ?'
                cursor.execute(sql, (msg.nick.lower(),))
                result = cursor.fetchone()
                if result == None:
                    irc.error("You are not registered. Request a new user account from gluon.", Raise=True)
                user_id = result[0]
                sql = 'INSERT INTO bets VALUES(NULL, "0", "0", ?, ?, ?, ?, ?, 0)'
                cursor.execute(sql, (race.lower(),
                                     first.lower(),
                                     second.lower(),
                                     third.lower(),
                                     str(user_id)))
                conn.commit()
            except sqlite3.Error:
                irc.error("Problem accessing the database.", Raise=True)
            finally:
                conn.close()
            irc.reply("Your bet for the " + ircutils.bold(race[12:len(race)-3] + " GP ") + \
                      "was successfully " + ircutils.bold("placed") + ".")
        else:
            # Bet found for the current race. Update the existing bet.
            try:
                conn = sqlite3.connect(self.database_path)
                cursor = conn.cursor()
                sql = 'UPDATE bets SET first = ?, ' \
                      'second = ?, third = ? ' \
                      'WHERE user_id = ? AND ' \
                      'race = ? COLLATE NOCASE'
                cursor.execute(sql, (first.lower(),
                                     second.lower(),
                                     third.lower(),
                                     str(user_id),
                                     race.lower()))
                conn.commit()
            except sqlite3.Error:
                irc.error("Problem accessing the database.", Raise=True)
            finally:
                conn.close()
            irc.reply("Your bet for the " + ircutils.bold(race[12:len(race)-3] + " GP ") + \
                      "was successfully " + ircutils.bold("updated") + ".")
            
    def commands(self, irc, msg, args):
        """

        Returns a list of all commands.
        """
        irc.reply(ircutils.bold("!ace | !bet | !commands | !lt | !next | !pc | !points | !porn | "
                                "!results | !top | !winners | !wbc"))
        irc.reply(ircutils.bold("Examples:"))
        irc.reply(ircutils.bold("!ace list -> list all streams"))
        irc.reply(ircutils.bold("!ace text_to_select_a_stream (f1, fp2, qualifying, etc) -> actually show stream url"))
        irc.reply(ircutils.bold("!next category (f1, f2, wec, motogp, etc) -> show next session"))
        irc.reply(ircutils.bold("!wbc -> show betting points"))

    def data(self, irc, msg, args):
        """[category]

        Returns the next motosport events.
        Examples: !next | !next f1 | !next indycar | !next wec | !next motogp |
        !next today | !next tomorrow |!.next friday | !next saturday | !next sunday | !next weekend
        """
        try:
            conn = sqlite3.connect(self.database_path)
            cursor = conn.cursor()
            sql = 'SELECT tz FROM users WHERE nick = ?'
            cursor.execute(sql, (msg.nick.lower(),))
            result = cursor.fetchone()
        except sqlite3.Error:
            irc.error("Problem accessing the database.", Raise=True)
        irc.reply("This command shows potentially sensitive data, check your query with the bot to see the actual data.")
        irc.reply("This is all the data I've gathered about user " + msg.nick + "-> timezone/city: " + result[0], private=True)

    def lt(self, irc, msg, args, category):
        """[category]

        Returns live timing links for motorsport events.
        Examples: !lt | !lt f1 | !lt indycar | !lt wec | !lt motogp
        """
        lt_url = "https://timing.71wytham.org.uk/"
        if category:
            irc.reply(ircutils.bold(category.upper() + " live timing: ") + \
                      lt_url + "s/" + category)
        else:
            irc.reply(ircutils.bold("Motorsports live timing: ") + lt_url)

    def next(self, irc, msg, args, category):
        """[category]

        Returns the next motosport events.
        Examples: !next | !next f1 | !next indycar | !next wec | !next motogp | 
        !next today | !next tomorrow |!.next friday | !next saturday | !next sunday | !next weekend
        """
        try:
            conn = sqlite3.connect(self.database_path)
            cursor = conn.cursor()
            if category:
                if category.lower() == 'today':
                    sql = 'SELECT dtstart, summary FROM events ' \
                          'WHERE dtstart > datetime("now") AND ' \
                          'dtstart < datetime("now", "start of day", "+1 day") ' \
                          'ORDER BY dtstart LIMIT 15'
                    cursor.execute(sql)
                elif category.lower() == 'tomorrow':
                    sql = 'SELECT dtstart, summary FROM events ' \
                          'WHERE dtstart > date("now", "start of day", "+1 day") AND ' \
                          'dtstart < date("now", "start of day", "+2 day") ' \
                          'ORDER BY dtstart LIMIT 15'
                    cursor.execute(sql)
                elif category.lower() == 'friday':
                    sql = 'SELECT dtstart, summary FROM events ' \
                          'WHERE dtstart > date("now", "weekday 5") AND ' \
                          'dtstart < date("now", "weekday 5", "+1 day") ' \
                          'ORDER BY dtstart LIMIT 15'
                    cursor.execute(sql)
                elif category.lower() == 'saturday':
                    sql = 'SELECT dtstart, summary FROM events ' \
                          'WHERE dtstart > date("now", "weekday 6") AND ' \
                          'dtstart < date("now", "weekday 6", "+1 day") ' \
                          'ORDER BY dtstart LIMIT 15'
                    cursor.execute(sql)
                elif category.lower() == 'sunday':
                    sql = 'SELECT dtstart, summary FROM events ' \
                          'WHERE dtstart > date("now", "weekday 0") AND ' \
                          'dtstart < date("now", "weekday 0", "+1 day") ' \
                          'ORDER BY dtstart LIMIT 15'
                    cursor.execute(sql)
                elif category.lower() == 'weekend':
                    sql = 'SELECT dtstart, summary FROM events ' \
                          'WHERE dtstart > date("now", "weekday 5") AND ' \
                          'dtstart < date("now", "weekday 5", "+3 day") AND ' \
                          '(summary LIKE "%event%" OR summary LIKE "%race" OR summary LIKE "%Race _") ' \
                          'ORDER BY dtstart LIMIT 15'
                    cursor.execute(sql)
                else:
                    category = self._lookup(category, 'category')
                    sql = 'SELECT dtstart, summary FROM events ' \
                          'WHERE dtstart > datetime("now") AND ' \
                          'summary LIKE ? ' \
                          'ORDER BY dtstart LIMIT 1'
                    cursor.execute(sql, ('%'+category.replace('*', '%')+'%',))
            else:
                sql = 'SELECT dtstart, summary FROM events ' \
                      'WHERE dtstart > datetime("now") ' \
                      'ORDER BY dtstart LIMIT 1'
                cursor.execute(sql)
            results = cursor.fetchall()
            user_tz = self.timezone
            sql = 'SELECT tz FROM users WHERE nick = ?'
            cursor.execute(sql, (msg.nick.lower(),))
            result = cursor.fetchone()
            if result:
                user_tz = result[0]
        except sqlite3.Error:
            irc.error("Problem accessing the database.", Raise=True)
        finally:
            conn.close()
        if results:
            for i in results:
                dt = datetime.datetime.strptime(i[0], '%Y-%m-%d %H:%M:%S UTC')
                tz = pytz.timezone('UTC')
                dt = dt.replace(tzinfo=tz)
                dt = dt.astimezone(pytz.timezone(user_tz))
                offset = dt.utcoffset().seconds // 3600
                hour = str(dt.hour).zfill(2)
                minute = str(dt.minute).zfill(2)
                delta = self._delta(i[0])
                output = calendar.day_name[dt.weekday()] + ", " + \
                         str(dt.day) + " " + \
                         calendar.month_name[dt.month] + " "
                if 'event' not in i[1].lower():
                    output += "at " + hour + ":" + minute + " " + \
                              ircutils.bold(dt.tzname() + " (UTC+" + str(offset) + ") ")
                output += "| " + i[1] + " | " + \
                          str(delta['days']) + " day(s), " + \
                          str(delta['hours']) + " hour(s), " + \
                          str(delta['minutes']) + " minute(s)"
                irc.reply(output)
        else:
            irc.reply("Nothing found.")

    def pc(self, irc, msg, args):
        """

        Returns links to the latest F1 press conferences.
        Examples: !pc
        """
        reddit = praw.Reddit(client_id=self.client_id,
                             client_secret=self.client_secret,
                             user_agent=self.user_agent)
        try:
            submissions = []
            for i in reddit.subreddit('formula1').search('press conference', sort='new'):
                if 'youtube.com' in i.url:
                    submissions.append(i)
            if submissions:
                irc.reply(submissions[1].title + " - " + submissions[1].url)
                irc.reply(submissions[0].title + " - " + submissions[0].url)
            else:
                irc.reply("Nothing found.")
        except Exception:
            irc.reply("Nothing found.")

    def points(self, irc, msg, args, championship):
        """<championship>

        Returns the championship standings.
        Examples: !points | !points wdc | !points wcc 
        """
        if not championship:
            championship = 'bets'
        elif championship.lower() == 'drivers' or championship.lower() == 'wdc':
            championship = 'driver'
        elif championship.lower() == 'constructors' or championship.lower() == 'wcc':
            championship = 'constructor'
        if championship.lower() == 'bets':
            try:
                conn = sqlite3.connect(self.database_path)
                cursor = conn.cursor()
                sql = 'SELECT nick, points FROM users ORDER BY points DESC, nick ASC'
                cursor.execute(sql)
                results = cursor.fetchall()
            except sqlite3.Error:
                irc.error("Problem accessing the database.", Raise=True)
            finally:
                conn.close()
            if results:
                output = ircutils.bold("Betting Championship: ")
                for index, i in enumerate(results):
                    if i[1] > 0:
                        output += ircutils.bold(str(index + 1)) + ". " + i[0].lstrip('()[]-_')[:3].upper() + " " + str(i[1]) + " | "
                irc.reply(output[:-3])
        else:
            r = requests.get(self.ergast_url + 'current' + '/' + championship + 'Standings' + '.json')
            try:
                results = r.json()
            except ValueError:
                irc.error("Invalid championship.", Raise=True)
            try:
                if championship.lower() == 'driver':
                    leader_points = results['MRData']['StandingsTable']['StandingsLists'][0]['DriverStandings'][0]['points']
                    output = ircutils.bold("Drivers Championship: ")
                    for i in results['MRData']['StandingsTable']['StandingsLists'][0]['DriverStandings']:
                        output += ircutils.bold(i['position'] + ". ") + \
                                  i['Driver']['givenName'][0].upper() + ". " + \
                                  i['Driver']['familyName'] + " " + \
                                  i['points'] + "/-" + str(int(leader_points) - int(i['points'])) + " (" + i['wins'] + " wins) | "
                elif championship.lower() == 'constructor':
                    leader_points = results['MRData']['StandingsTable']['StandingsLists'][0]['ConstructorStandings'][0]['points']
                    output = ircutils.bold("Constructors Championship: ")
                    for i in results['MRData']['StandingsTable']['StandingsLists'][0]['ConstructorStandings']:
                        output += ircutils.bold(i['position'] + ". ") + \
                                  i['Constructor']['name'] + " " + \
                                  i['points'] + "/-" + str(int(leader_points) - int(i['points'])) + " (" + i['wins'] + " wins) | "
            except IndexError:
                irc.error("Invalid championshp.", Raise=True)
            irc.reply(output[:-3].replace('/-0', ''))

    def porn(self, irc, msg, args, category, query):
        """[category] [query]

        Returns random porn pictures from the given category.
        Examples: !porn | !porn f1 | !porn indy | !porn wec | !porn f1 senna
        """
        reddit = praw.Reddit(client_id=self.client_id,
                             client_secret=self.client_secret,
                             user_agent=self.user_agent) 
        if not category:
            category = 'f1'
        if category == 'indy':
            category = 'indycar'
        if category and category not in ('f1', 'indycar', 'motogp', 'wec') and not query:
            query = category
            category = 'f1'
        if not query:
            try:
                submission = reddit.subreddit(category.strip() + u'porn').random()
                irc.reply(submission.title + " - " + submission.url)
            except Exception:
                irc.reply("Nothing found.")
        else:
            try:
                submissions = []
                for i in reddit.subreddit(category.strip() + u'porn').search(query):
                    submissions.append(i)
                i = random.randint(0, len(submissions)-1)
                if submissions:
                    irc.reply(submissions[i].title + " - " + submissions[i].url)
                else:
                    irc.reply("Nothing found.")
            except Exception:
                irc.reply("Nothing found.")

    def pu(self, irc, msg, args, driver):
        """<driver>

        Returns the number of used power unit elements.
        Examples: !pu alonso | !pu ham
        """
        try:
            conn = sqlite3.connect(self.database_path)
            cursor = conn.cursor()
            if driver:
                driver = self._lookup(driver, 'driver')
                sql = 'SELECT driver, ice, tc, mgu_h, mgu_k, es, ce FROM pu_elements ' \
                      'WHERE driver = ?'
                cursor.execute(sql, (driver,))
                result = cursor.fetchone()
        except sqlite3.Error:
            irc.error("Problem accessing the database.", Raise=True)
        finally:
            conn.close()
        if result:
            driver = result[0]
            ice = result[1]
            tc = result[2]
            mgu_h = result[3]
            mgu_k = result[4]
            es = result[5]
            ce = result[6]
            irc.reply(ircutils.bold("Driver: ") + driver[0:3].upper() + " | " +
                      ircutils.bold("ICE: ") + str(ice) + " | " +
                      ircutils.bold("TC: ") + str(tc) + " | " +
                      ircutils.bold("MGU-H: ") + str(mgu_h) + " | " +
                      ircutils.bold("MGU-K: ") + str(mgu_k) + " | " +
                      ircutils.bold("ES: ") + str(es) + " | " +
                      ircutils.bold("CE: ") + str(ce))
        else:
            irc.reply("Nothing found.")

    def results(self, irc, msg, args, session, year, race, verbose):
        """[session] [year] [event] [verbose]

        Returns F1 results.
        Examples: !results | !results qualy | !results race 1995 portugal | !results qualy 2017 monaco true
        """
        if not session:
            session = 'results'
        elif session.lower() == 'qualy' or session.lower() == 'qualifying':
            session = 'qualifying'
            verbose = True
        elif session.lower() == 'race':
            session = 'results'
        else:
            irc.error("Session must be either qualifying or race.", Raise=True)
        if not year:
            year = 'current'
        if not race:
            if session == 'qualifying':
                race = self._current_race()
            else:
                race = 'last'
            r = requests.get(self.ergast_url + year + '/' + race + '/' + session + '.json')
        else:
            if race.isdigit():
                r = requests.get(self.ergast_url + year + '/' + race + '/' + session + '.json')
            else:
                r = requests.get(self.ergast_url + year + '/' + session + '.json?limit=1000')
        try:
            results = r.json()
        except ValueError:
            irc.error("Invalid year, round or session.", Raise=True)
        empty = True
        try:
            output = ircutils.bold(results['MRData']['RaceTable']['season'] + " " + \
                                   race[0].upper() + race[1:].lower()  + " ")
            if session == 'qualifying':
                if race == 'last' or race.isdigit():
                    empty = False
                    output += ircutils.bold("(Qualifying): ")
                    for i in results['MRData']['RaceTable']['Races'][0]['QualifyingResults']:
                        output += ircutils.bold(i['position'] + ". ") + \
                                  i['Driver']['givenName'][0] + ". " + \
                                  i['Driver']['familyName'] + ' | '
                        if verbose and 'Q3' in i:
                            output = output[:-2] + i['Q3'] + ' | '
                else:
                    for i in results['MRData']['RaceTable']['Races']:
                        if (race.lower() == i['Circuit']['Location']['country'].lower() or
                            race.lower() == i['Circuit']['Location']['locality'].lower()):
                            empty = False
                            output += ircutils.bold("(Qualifying): ")
                            for j in i['QualifyingResults']:
                                output += ircutils.bold(j['position'] + ". ") + \
                                          j['Driver']['givenName'][0] + ". " + \
                                          j['Driver']['familyName'] + ' | '
                                if verbose and 'Q3' in j:
                                    output = output[:-2] + j['Q3'] + ' | '
            elif session == 'results':
                if race == 'last':
                    empty = False
                    output += ircutils.bold("(Race): ")
                    for i in results['MRData']['RaceTable']['Races'][0]['Results']:
                        output += ircutils.bold(i['position'] + ". ") + \
                                  i['Driver']['givenName'][0] + ". " + \
                                  i['Driver']['familyName'] + ' | '
                        if verbose and 'Time' in i:
                            output = output[:-2] + i['Time']['time'] + ' | '
                else:
                    for i in results['MRData']['RaceTable']['Races']:
                        if (race.lower() == i['Circuit']['Location']['country'].lower() or
                            race.lower() == i['Circuit']['Location']['locality'].lower()):
                            empty = False
                            output += ircutils.bold("(Race): ")
                            for j in i['Results']:
                                output += ircutils.bold(j['position'] + ". ") + \
                                          j['Driver']['givenName'][0] + ". " + \
                                          j['Driver']['familyName'] + ' | '
                                if verbose and 'Time' in j:
                                    output = output[:-2] + j['Time']['time'] + ' | '
        except IndexError:
            irc.error("Invalid year, round or session.", Raise=True)
        if not empty:
            irc.reply(output[:-3])
        else:
            irc.reply("Nothing found.")

    def starting(self, irc, msg, args):
        """

        Returns the motosport event that is starting.
        """
        try:
            conn = sqlite3.connect(self.database_path)
            cursor = conn.cursor()
            sql = 'SELECT dtstart, summary FROM events ' \
                  'WHERE dtstart > datetime("now") AND ' \
                  'dtstart < datetime("now", "+5 Minute") AND ' \
                  'summary NOT LIKE "%event%" ' \
                  'LIMIT 1'
            cursor.execute(sql)
            result = cursor.fetchone()
        except sqlite3.Error:
            irc.error("Problem accessing the database.", Raise=True)
        finally:
            conn.close()
        if result:
            with open(self.events_announced_path, 'r+a') as f:
                data = f.read()
                if result[0] + ' ' + result[1] not in data:
                    irc.reply(ircutils.mircColor("Starting in 5 minutes: ", 'red') + \
                              result[1], to='#formula1')
                    f.write(result[0] + ' ' + result[1] + '\n')
                    if result[1].lower().startswith('[formula 1]') and result[1].lower().endswith('gp qualifying'):
                        irc.reply(ircutils.mircColor("WARNING: 5 minutes left to bet. " + \
                                                "You will be betting for the next race after " + \
                                                result[0], 'red'), to='#formula1')
                    if result[1].lower().startswith('[formula 1]'):
                        try:
                            conn = sqlite3.connect(self.database_path)
                            cursor = conn.cursor()
                            sql = 'SELECT category, link FROM acestream_links ' \
                                  'WHERE link != "0" ' \
                                  'LIMIT 5'
                            cursor.execute(sql)
                            streams = cursor.fetchall()
                        except sqlite3.Error:
                            irc.error("Problem accessing the database.", Raise=True)
                        finally:
                            conn.close()
                        if streams:
                            irc.reply(ircutils.bold("Default streams (use !ace if outdated):"))
                            irc.reply(streams[0][0][0].upper() + streams[0][0][1:] + " - " + streams[0][1])

    def top(self, irc, msg, args, category, count, period):
        """[category] [count] [period]

        Returns the top links of the day, week, month, year or all time  for a given category.
        Examples: !top | !top f1 | !top indycar !top wec | !top f1 5 | !top f1 4 day | 
        !top f1 3 week | !top f1 2 month | !top f1 1 year | !top f1 1 all
        """
        reddit = praw.Reddit(client_id=self.client_id,
                             client_secret=self.client_secret,
                             user_agent=self.user_agent)
        if not category:
            category = 'formula1'
        if category == 'f1':
            category = 'formula1'
        if category == 'indy':
            category = 'indycar'
        if not count:
            count = 1
        if count < 1 or count > 5:
            irc.error("Count must be betwee 1 and 5.", Raise=True)
        if not period:
            period = 'day'
        if period not in ('day', 'week', 'month', 'year', 'all'):
            irc.error("Period must be either day, week, month, year or all.", Raise=True)
        subreddit = reddit.subreddit(category)
        try:
            for submission in subreddit.top(limit=10, time_filter=period):
                if not submission.stickied and count > 0:
                    if len(submission.title) > 60:
                        irc.reply(submission.title[:60] + "... - "  + submission.url)
                    else:
                        irc.reply(submission.title + " - "  + submission.url)
                    count -= 1
        except Exception:
            irc.error("Invalid category.", Raise=True)
        irc.reply("To see the betting points use: !wbc or !points")

    def videos(self, irc, msg, args, category):
        """<category>

        Returns videos from the given category.
        """
        reddit = praw.Reddit(client_id=self.client_id,
                             client_secret=self.client_secret,
                             user_agent=self.user_agent)
        with open(self.videos_announced_path, 'r+a') as f:
            data = f.read()
            if category and self._lookup(category, 'category') == 'formula 1':
                for submission in reddit.redditor('BottasWMR').submissions.new(limit=10):
                    if ('streamable.com' in submission.url.lower() or \
                        '.mp4' in submission.url.lower()) and \
                       (submission.url.lower() not in data):
                        irc.reply("[" + ircutils.bold("F1 VIDEOS") + "] " + \
                                  submission.title + " - " + \
                                  submission.url, to='#formula1')
                        f.write(submission.url.lower() + '\n')

    def winners(self, irc, msg, args, year):
        """[year]

        Returns season winners.
        Examples: !winners 1994 | !winners 2010 | !winners 2017
        """
        if not year:
            year = 'current'
        r = requests.get(self.ergast_url + str(year) + '/' + 'results' + '.json?limit=1000')
        try:
            results = r.json()
        except ValueError:
            irc.error("Invalid year.", Raise=True)
        try:
            output = ircutils.bold(results['MRData']['RaceTable']['season'] + " ")
            for i in results['MRData']['RaceTable']['Races']:
                output += ircutils.bold(i['Circuit']['Location']['country'] + ": ") + \
                          i['Results'][0]['Driver']['givenName'][0] + ". " + \
                          i['Results'][0]['Driver']['familyName'] + ' | '
        except IndexError:
            irc.error("Invalid year.", Raise=True)
        irc.reply(output[:-3])

    ace = wrap(ace, [optional('text')])
    ask = wrap(ask, ['text'])
    bet = wrap(bet, [optional('somethingWithoutSpaces'), optional('somethingWithoutSpaces'), optional('somethingWithoutSpaces')])
    commands = wrap(commands)
    data = wrap(data)
    kimi = wrap(kimi)
    lt = wrap(lt, [optional('text')])
    next = wrap(next, [optional('text')])
    pc = wrap(pc)
    points = wrap(points, [optional('somethingWithoutSpaces')])
    porn = wrap(porn, [optional('somethingWithoutSpaces'), optional('somethingWithoutSpaces')])
    pu = wrap(pu, ['somethingWithoutSpaces'])
    results = wrap(results, [optional('somethingWithoutSpaces'), optional('somethingWithoutSpaces'), optional('somethingWithoutSpaces'), optional('boolean')])
    starting = wrap(starting)
    top = wrap(top, [optional('somethingWithoutSpaces'), optional('int'), optional('somethingWithoutSpaces')])
    videos = wrap(videos, [optional('text')])
    winners = wrap(winners, [optional('int')])

Class = Motorsport


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=100:
